package main

import (
	"encoding/json"
	"strings"
	"sync"

	"github.com/go-redis/redis/v8"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gsheng19/fyp/worker-mcq/models"
	"gitlab.com/gsheng19/fyp/worker-mcq/util"
)

var counter int

func main() {
	err := godotenv.Load() // Check for environment file
	if err != nil {
		log.Println(".env file not found")
	}

	if util.GetEnvDefaultInt("DEBUG", 0) == 1 {
		log.SetLevel(log.DebugLevel)
		log.Debugln("DEBUG MODE ENABLED")
	}

	redisClient := util.SetupRedisClient()

	log.Println("Setting up redis client, ready for new jobs")
	var wg sync.WaitGroup
	maxJobs := util.GetEnvDefaultInt("JOB_LIMIT", 100)
	timeout := util.GetEnvDefaultInt("TIMEOUT", 3600) // Timeout in seconds
	log.Println("Max Jobs for Worker:", maxJobs, "| Timeout:", timeout, "seconds")

	counter = 0

	for {
		// Only allow certain number of simultaneous jobs
		if counter >= maxJobs {
			log.Println("Waiting for active jobs to finish...")
			wg.Wait()
			counter = 0
		}

		// Blocking pop to await for jobs
		res, err := redisClient.BLPop(util.BgCtx, 0, util.GetEnvDefault("QUEUE", "mcqqueue")).Result()
		if err != nil {
			panic(err)
		}
		log.Debugln(res[1])

		// Process the json
		var input models.QueueObj
		err3 := json.Unmarshal([]byte(res[1]), &input)
		if err3 != nil {
			log.Errorln(err3)
			log.Warnln("Ignoring...")
			continue
		}

		// Get relevant data out from DB from key
		// TODO: Reimplement

		data, answers := util.GetSubmissionAttemptAsRedis(input.Key)
		if data == nil || answers == nil {
			log.Warnln("THERE ARE EITHER NO SUBMISSION OR ANSWERS FOR", input.Key)
			updateSubmissionAttempt(redisClient, input.Key, 0)
			continue
		}
		log.Debugln(answers)
		log.Debugln(data)
		// Process the submission
		//// Save data to redis for update
		wg.Add(1)
		counter++
		go goProcessSubmission(&wg, data, answers, input.Key, redisClient)
	}
}

// Push to the submission queue for the submission worker to pick up and validate marked state
func updateSubmissionAttempt(client *redis.Client, aid string, score float64) {
	// Update MySQL DB
	util.UpdateSubmissionAttempt(aid, score)
	log.Println("Submission for Attempt #", aid, "updated successfully")
	client.RPush(util.BgCtx, util.GetEnvDefault("QUEUE_SUBMISSION", "submissionqueue"), "c-"+aid) // Push a check key back to submission queue
}

// Concurrently process submissions mark
func goProcessSubmission(wg *sync.WaitGroup, userAnswer *models.UserAnswer, actualAnswers []models.ActualAnswers, key string, client *redis.Client) {
	defer wg.Done()

	totalScore := 0.0

	for _, answer := range actualAnswers {
		actans := answer.Answer
		usrans := userAnswer.AnswerText
		if actans != "" {
			log.Println("strings.contains = ", strings.Contains(usrans, actans))
			if strings.Contains(usrans, actans) { //check if the current loop is in the user's choice
				log.Println("answer.Correct = ", answer.Correct)
				if answer.Correct {
					//log.Println("do correct answer action here")
				}
				choiceScore, err := answer.Score.Float64()
				if err != nil {
					log.Errorln("Error getting score")
					choiceScore = 0.0
				}
				log.Println("choiceScore = ", choiceScore)
				totalScore += choiceScore
				log.Println("total score = ", totalScore)
			}
		} else {
			totalScore = 0
		}
	}

	// Minimum Score is 0
	if totalScore < 0 { //if total Score is negative
		totalScore = 0
	}

	MaxScore, _ := userAnswer.MaxScoreQn.Float64()
	if totalScore > MaxScore {
		totalScore = MaxScore
	}

	log.Debugf("Final Score for question: %f", totalScore)
	updateSubmissionAttempt(client, key, totalScore)
}
