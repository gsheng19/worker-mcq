image: alpine:latest

stages:
  - build
  - test
  - Docker Image Build
  - deploy
  - Deploy Staging

# Workflow Default Settings
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Secret-Detection.gitlab-ci.yml
  - template: Container-Scanning.gitlab-ci.yml
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: License-Scanning.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml

license_scanning:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success
  needs: []

code_quality:
  artifacts:
    paths: [gl-code-quality-report.json]
  rules:
    - if: '$CODE_QUALITY_DISABLED'
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success
  needs: []

code_quality_html:
  extends: code_quality
  variables:
    REPORT_FORMAT: html
  artifacts:
    paths: [gl-code-quality-report.html]
  rules:
    - if: '$CODE_QUALITY_DISABLED'
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success
  needs: []

secret_detection:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success
  needs: []

# secret_detection_default_branch:
#   rules:
#     - if: $CI_COMMIT_TAG
#       when: never
#     - when: on_success
#   needs: [ ]

gosec-sast:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success
  needs: [ ]

container_scanning:
  dependencies:
    - Build Docker
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success
  needs: ["Build Docker"]

gemnasium-dependency_scanning:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success
  needs: []


# Cleaning up to make the container registry neater
Cleanup Container Scan Docker Image:
  image: docker:latest
  stage: deploy
  services:
    - docker:dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
    REG_SHA256: ade837fc5224acd8c34732bf54a94f579b47851cc6a7fd5899a98386b782e228
    REG_VERSION: 0.16.1
  before_script:
    - apk add --no-cache curl
    - curl --fail --show-error --location "https://github.com/genuinetools/reg/releases/download/v$REG_VERSION/reg-linux-amd64" --output /usr/local/bin/reg
    - echo "$REG_SHA256  /usr/local/bin/reg" | sha256sum -c -
    - chmod a+x /usr/local/bin/reg
  script:
    - /usr/local/bin/reg rm -d --auth-url $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $IMAGE_TAG || true
  needs: ["container_scanning"]

Install BuildX:
  image: docker:git
  stage: build
  variables:
    GIT_STRATEGY: none
  artifacts:
    paths:
      - buildx
    expire_in: 6h
  services:
    - docker:dind
  script:
    - export DOCKER_BUILDKIT=1
    - git clone git://github.com/docker/buildx ./docker-buildx
    - docker build --platform=local -o . ./docker-buildx
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: always
    - if: '$CI_COMMIT_TAG != null'
      when: always
    - when: never

# Build docker containers
.install_buildx: &installbuildx
  # Official docker image.
  image: docker:latest
  dependencies:
    - Install BuildX
  services:
    - name: docker:dind
      command: ["--experimental"]
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - mkdir -p ~/.docker/cli-plugins
    - mv buildx ~/.docker/cli-plugins/docker-buildx
    - docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
    - docker buildx create --use --driver docker-container --name mybuilder
    - docker buildx inspect --bootstrap

.gen_docker_images_master: &gendockerimagesmaster
  <<: *installbuildx
  stage: Docker Image Build
  script:
    - docker buildx build --platform $PLATFORMS --progress=plain --push -t $IMAGE_TAG .
  needs: ["Install BuildX"]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: always
    - when: never

.gen_docker_images_tags: &gendockerimagestags
  <<: *installbuildx
  stage: Docker Image Build
  script:
    - docker buildx build --platform $PLATFORMS --progress=plain --push -t $IMAGE_TAG .
  needs: ["Install BuildX"]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CI_COMMIT_TAG != null'
      when: always
    - when: never

Build ARMv6 Docker Image Master:
  <<: *gendockerimagesmaster
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:armv6
    PLATFORMS: linux/arm/v6
  timeout: 3h

Build ARMv7a Docker Image Master:
  <<: *gendockerimagesmaster
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:armv7a
    PLATFORMS: linux/arm/v7
  timeout: 3h

Build ARM64 Docker Image Master:
  <<: *gendockerimagesmaster
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:arm64
    PLATFORMS: linux/arm64

Build x86 Docker Image Master:
  <<: *gendockerimagesmaster
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:x86
    PLATFORMS: linux/386

Build x86_64 Docker Image Master:
  <<: *gendockerimagesmaster
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:x64
    PLATFORMS: linux/amd64

Docker Merge Manifest Master:
  <<: *installbuildx
  stage: deploy
  dependencies:
    - Install BuildX
    - Build ARMv6 Docker Image Master
    - Build ARMv7a Docker Image Master
    - Build ARM64 Docker Image Master
    - Build x86 Docker Image Master
    - Build x86_64 Docker Image Master
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE
  script:
    - docker buildx imagetools create -t $IMAGE_TAG $IMAGE_TAG:x64 $IMAGE_TAG:x86 $IMAGE_TAG:arm64 $IMAGE_TAG:armv7a $IMAGE_TAG:armv6
    - docker buildx imagetools inspect $IMAGE_TAG
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: always
    - when: never

Build Docker:
  # Official docker image.
  image: docker:latest
  stage: build
  services:
    - name: docker:dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    CONTAINER_TAG: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t $IMAGE_TAG -t $CONTAINER_TAG .
    - docker push $IMAGE_TAG
    - docker push $CONTAINER_TAG

Build ARMv6 Docker Image Tagged:
  <<: *gendockerimagestags
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE/releases:$CI_COMMIT_REF_SLUG-armv6
    PLATFORMS: linux/arm/v6
  timeout: 3h

Build ARMv7a Docker Image Tagged:
  <<: *gendockerimagestags
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE/releases:$CI_COMMIT_REF_SLUG-armv7a
    PLATFORMS: linux/arm/v7
  timeout: 3h

Build ARM64 Docker Image Tagged:
  <<: *gendockerimagestags
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE/releases:$CI_COMMIT_REF_SLUG-arm64
    PLATFORMS: linux/arm64

Build x86 Docker Image Tagged:
  <<: *gendockerimagestags
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE/releases:$CI_COMMIT_REF_SLUG-x86
    PLATFORMS: linux/386

Build x86_64 Docker Image Tagged:
  <<: *gendockerimagestags
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE/releases:$CI_COMMIT_REF_SLUG-x64
    PLATFORMS: linux/amd64

Docker Merge Manifest Tagged:
  <<: *installbuildx
  stage: deploy
  dependencies:
    - Install BuildX
    - Build ARMv6 Docker Image Tagged
    - Build ARMv7a Docker Image Tagged
    - Build ARM64 Docker Image Tagged
    - Build x86 Docker Image Tagged
    - Build x86_64 Docker Image Tagged
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE/releases:$CI_COMMIT_REF_SLUG
  script:
    - docker buildx imagetools create -t $IMAGE_TAG $IMAGE_TAG-x64 $IMAGE_TAG-x86 $IMAGE_TAG-arm64 $IMAGE_TAG-armv7a $IMAGE_TAG-armv6
    - docker buildx imagetools inspect $IMAGE_TAG
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CI_COMMIT_TAG != null'
      when: always
    - when: never

Cleanup GitLab Container Repository:
  image: registry.gitlab.com/itachi1706/gitlab-container-registry-repository-cleaner:latest
  stage: deploy
  variables:
    GITLAB_TOKEN: $GITLAB_PAT
    GITLAB_PROJECT: $CI_PROJECT_ID
    GIT_STRATEGY: none
  before_script:
    - cd /usr/src/app
  script: npm start
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: always
    - when: never

SCSE Server:
  stage: Deploy Staging
  before_script:
    - echo "Deploying to Staging Server"
    - cd /home/VMuser/prod
  variables:
    GIT_STRATEGY: none
  script:
    - docker-compose pull
    - docker-compose up -d
  after_script:
    - echo "Deployment successful"
  tags:
    - scse-deploy-staging
  environment:
    name: staging
    url: http://172.21.148.188/
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: manual
    - if: '$CI_COMMIT_TAG != null'
      when: manual
    - when: never

Kubernetes Cluster:
  stage: Deploy Staging
  image: bitnami/kubectl
  before_script:
    - echo "Deploying to Kubernetes Cluster"
  script:
    - sed -i "s/CI_PROJECT_PATH_SLUG/$CI_PROJECT_PATH_SLUG/g" cik8sdeploy.yaml
    - sed -i "s/CI_ENVIRONMENT_SLUG/$CI_ENVIRONMENT_SLUG/g" cik8sdeploy.yaml
    - kubectl apply -f cik8sdeploy.yaml -n $KUBE_NAMESPACE
    - kubectl rollout restart deployment worker-mcq
  after_script:
    - echo "Deployment successful"
  tags:
    - cluster
    - kubernetes
  environment:
    name: k8s
    url: https://web.aaspk8s.itachi1706.com
    kubernetes:
      namespace: aasp
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: manual
    - if: '$CI_COMMIT_TAG != null'
      when: manual
    - when: never
