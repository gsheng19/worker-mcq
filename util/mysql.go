package util

import (
	"database/sql"
	"encoding/json"
	"regexp"

	"strconv"

	_ "github.com/go-sql-driver/mysql"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gsheng19/fyp/worker-mcq/models"
)

func ConnectMySQL() *sql.DB {
	username := GetEnvDefault("MYSQL_USER", "")
	password := GetEnvDefault("MYSQL_PASSWORD", "")
	port := GetEnvDefault("MYSQL_PORT", "3306")
	host := GetEnvDefault("MYSQL_HOST", "localhost")
	name := GetEnvDefault("MYSQL_DATABASE", "aasp")

	db, err := sql.Open("mysql", username+":"+password+"@tcp("+host+":"+port+")/"+name)
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	return db
}

var re1 = regexp.MustCompile(`"(.*?)"`)

func getQuotedStrings1(s string) []string {
	ms := re1.FindAllStringSubmatch(s, -1)
	ss := make([]string, len(ms))
	for i, m := range ms {
		ss[i] = m[1]
	}
	return ss
}

// Get Submission attempt from DB and push to Redis
func GetSubmissionAttemptAsRedis(id string) (*models.UserAnswer, []models.ActualAnswers) {
	db := ConnectMySQL()
	defer db.Close()

	userAns := models.UserAnswer{AnswerText: ""}
	stmtGen, err := db.Prepare("SELECT qq.attemptid, qn.questionid, qq.answer, qn.id FROM quiz_attempt_qn qq JOIN quiz_questions qn ON qq.questionid=qn.id WHERE qq.id=?")
	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}
	defer stmtGen.Close()
	var aid int
	var qid int
	var qnid int

	err = stmtGen.QueryRow(id).Scan(&aid, &qid, &userAns.AnswerText, &qnid)

	log.Println("&userAns.AnswerText: ", &userAns.AnswerText)
	log.Println("aid = ", aid, " qid = ", qid, " qnid = ", qnid, " userAns = ", userAns, " err = ", err)

	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}

	// Get max score
	stmtScore, err := db.Prepare("SELECT q.maxscore FROM quiz_questions qn JOIN question q ON qn.questionid=q.id WHERE qn.id=?")
	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}
	defer stmtScore.Close()
	var maxScore sql.NullString
	err = stmtScore.QueryRow(qnid).Scan(&maxScore)
	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}
	if maxScore.Valid {
		userAns.MaxScoreQn = json.Number(maxScore.String)
	} else {
		userAns.MaxScoreQn = "0"
	}

	mScore, _ := userAns.MaxScoreQn.Float64()
	log.Println("Max Score: ", mScore)

	// Get keywords  correctansRows is actual correct answer
	correctansRows, err := db.Query("SELECT answer,iscorrect,rationale,score FROM question_mcq_ans WHERE questionid=?", qid)
	log.Println("correctansRows.answer = ", correctansRows)
	log.Println("&correctansRows = ", &correctansRows)

	if err != nil {
		log.Errorln(err.Error())
		return &userAns, nil
	}

	defer correctansRows.Close()

	var correctAns []models.ActualAnswers

	for correctansRows.Next() {
		var kw models.ActualAnswers
		var isCorrectInt int
		var rationale string
		var score sql.NullString
		err = correctansRows.Scan(&kw.Answer, &isCorrectInt, &rationale, &score)
		if err != nil {
			log.Println("error in correctansRows.scan!!")
			log.Errorln(err.Error())
			continue
		}
		log.Println("&kw.Answer = ", &kw.Answer, " &rationale = ", &rationale, " &isCorrectInt = ", &isCorrectInt, " &score = ", &score)
		if score.Valid {
			kw.Score = json.Number(score.String)
			log.Println("kw.Score = ", kw.Score)
		} else {
			// divide and round up
			kw.Score = json.Number(strconv.FormatFloat(mScore, 'e', 2, 64))
		}
		kw.Correct = isCorrectInt == 1

		correctAns = append(correctAns, kw) // Add to keywords
		log.Println("correctAns = ", correctAns)
	}

	log.Println("&userAns = ", &userAns, " correctAns = ", correctAns)
	return &userAns, correctAns
}

// Update Submission Attempt in DB for structured questions
func UpdateSubmissionAttempt(id string, score float64) bool {
	db := ConnectMySQL()
	defer db.Close()

	stmtMaxScore, err := db.Prepare("SELECT qn.maxscore FROM question qn JOIN quiz_questions qq ON qq.questionid = qn.id JOIN quiz_attempt_qn qa ON qa.questionid = qq.id WHERE qa.id=?")
	if err != nil {
		log.Errorln(err.Error())
		return false
	}
	defer stmtMaxScore.Close()

	stmtUpdateTestAttempt, err := db.Prepare("UPDATE quiz_attempt_qn SET score=?, ismarked=1 WHERE id=?")
	if err != nil {
		log.Errorln(err.Error())
		return false
	}
	defer stmtUpdateTestAttempt.Close()

	var maxscore json.Number
	err = stmtMaxScore.QueryRow(id).Scan(&maxscore)
	if err != nil {
		log.Errorln(err.Error())
		return false
	}
	maxscoreFloat, _ := maxscore.Float64()

	if score > maxscoreFloat {
		score = maxscoreFloat
	}

	log.Debugf("Score: %f | Submit: %f", maxscoreFloat, score)

	_, err = stmtUpdateTestAttempt.Exec(score, id)
	if err != nil {
		log.Errorln(err.Error())
		return false
	}
	return true
}
